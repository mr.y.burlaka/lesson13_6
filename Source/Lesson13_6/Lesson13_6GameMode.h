// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lesson13_6GameMode.generated.h"

UCLASS(minimalapi)
class ALesson13_6GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALesson13_6GameMode();
};



