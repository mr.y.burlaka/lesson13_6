// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lesson13_6GameMode.h"
#include "Lesson13_6Character.h"
#include "UObject/ConstructorHelpers.h"

ALesson13_6GameMode::ALesson13_6GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
