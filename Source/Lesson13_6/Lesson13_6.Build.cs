// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Lesson13_6 : ModuleRules
{
	public Lesson13_6(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
        PrivateDependencyModuleNames.AddRange(new string[] { });
    }
}
