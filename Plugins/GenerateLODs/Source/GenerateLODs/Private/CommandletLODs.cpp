// Fill out your copyright notice in the Description page of Project Settings.


#include "CommandletLODs.h"

#include "EditorStaticMeshLibrary.h"
#include "Engine/StaticMesh.h"

#include "AssetRegistryModule.h"
#include "Modules/ModuleManager.h"
//
//int32 UCommandletLODs::Main(const FString& Params)
//{
//	TArray<FString> Tokens;
//	TArray<FString> Switches;
//
//	ParseCommandLine(*Params, Tokens, Switches);
//	//-run=GenerateLODs -GenerateLod /Game/StarterContent/Shapes /Game/StarterContent/Props
//	//"C:\Program Files\Epic Games\UE_4.27\Engine\Binaries\Win64\UE4Editor.exe" "E:\Skillbox Project\Lesson13_6\Lesson13_6.uproject" -run=GenerateLODs -GenerateLod /Game/StarterContent/Shapes /Game/StarterContent/Props
//
//	if (Switches.Contains(TEXT("GenerateLod")))
//	{
//		if (Tokens.Num()>0)
//		{
//			ProcessAssets(Tokens);
//		}
//	}
//
//	return 0;
//}
//
//void UCommandletLODs::ProcessAssets(TArray<FString> RootDirectories)
//{
//	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(AssetRegistryConstants::ModuleName);
//
//	AssetRegistryModule.Get().SearchAllAssets(true);
//
//	FString ClassName = TEXT("StaticMesh");
//	TArray<FAssetData> AssetList;
//	AssetRegistryModule.Get().GetAssetsByClass(*ClassName, AssetList, true);
//
//	for (FAssetData AssetData : AssetList)
//	{
//		for (FString RootDirectories : RootDirectories)
//		{
//			if (AssetData.ObjectPath.ToString().StartsWith(RootDirectories, ESearchCase::IgnoreCase))
//			{
//				UObject* AssetInstance = AssetData.GetAsset();
//
//				ModifyLod(AssetInstance);
//
//				SaveAssets(AssetInstance);
//
//				break;
//			}
//		}
//	}
//
//}
//
//void UCommandletLODs::ModifyLod(UObject* AssetInstance)
//{
//	if (UStaticMesh* Mesh = Cast <UStaticMesh>(AssetInstance))
//	{
//		TArray<FEditorScriptingMeshReductionSettings> ReductionSettings;
//
//		FEditorScriptingMeshReductionSettings Settings;
//
//		// LOD0
//		Settings.PercentTriangles = 1.f;
//		Settings.ScreenSize = 0.75f;
//		ReductionSettings.Add(Settings);
//
//		// LOD1
//		Settings.PercentTriangles = 0.5f;
//		Settings.ScreenSize = 0.5f;
//		ReductionSettings.Add(Settings);
//
//		// LOD2
//		Settings.PercentTriangles = 0.1f;
//		Settings.ScreenSize = 0.3f;
//		ReductionSettings.Add(Settings);
//
//		FEditorScriptingMeshReductionOptions Options;
//		Options.ReductionSettings = ReductionSettings;
//
//		UEditorStaticMeshLibrary::SetLods(Mesh, Options);
//
//		AssetInstance->MarkPackageDirty();
//
//	}
//}
//
//void UCommandletLODs::SaveAssets(UObject* AssetInstance)
//{
//	if (AssetInstance)
//	{
//		if (UPackage* Package = AssetInstance->GetPackage())
//		{
//			if (Package->IsDirty())
//			{
//				FString PackageName = FPackageName::LongPackageNameToFilename(Package->GetPathName(),
//					FPackageName::GetAssetPackageExtension());
//
//				UE_LOG(LogClass, Log, TEXT("Saving Asset to : %s..."), *PackageName);
//				if (Package->SavePackage(Package, AssetInstance, RF_Standalone, *PackageName, GLog))
//				{
//					UE_LOG(LogClass, Log, TEXT("DONE,"));
//				}
//				else
//				{
//					UE_LOG(LogClass, Warning, TEXT("Cant save asset! "));
//				}
//			}
//		}
//	}
//}
