// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EditorStaticMeshLibrary.h"
#include "GenerateLODLibrary.generated.h"

/**
 * 
 */
UCLASS()
class GENERATELODS_API UGenerateLODLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
